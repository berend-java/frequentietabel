package main;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class MyFrame extends JFrame implements ActionListener {
	private JLabel lab;
	private JTextField tf;
	private JButton b;
	private JTextArea ta;
	private String frequentieString;
	public Frequentie fr = new Frequentie();

	public MyFrame() {
		setLayout(new FlowLayout());
		lab = new JLabel("Voer hier een text in");
		tf = new JTextField(10);
		tf.setEditable(true);
		tf.setBackground(Color.WHITE);
		b = new JButton("Frequentie tabel");
		ta = new JTextArea();

		add(lab);
		add(tf);
		add(b);
		b.addActionListener(this);
		add(ta);

		setSize(400, 150);
		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
	}

	public void actionPerformed(ActionEvent event) {
		String s = tf.getText();
		frequentieString = fr.drukFrequentieTabelAf(s);
		ta.setText(frequentieString);
	}
}
